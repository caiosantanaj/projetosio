package ServerPackage;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.crypto.KeyAgreement;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class User {

    private byte[] iv;
    private Map<String, PublicKey> publickeys = new ConcurrentHashMap<>();
    private List<Integer> usedPassword = new ArrayList<>();

    private final Scanner sc;
    private DataOutputStream outServer;
    private DataInputStream inServer;
    private final Socket socket;

    private final String ALGORITIMO = "RSA";
    private final int ALGORITIMO_LENGTH = 2048;

    private PrivateKey privateKey;
    private PublicKey publicKey;
    private PublicKey otherPubicKey;
    private SecretKey secretKey;
    private byte[] dhSecretKey;
    private String nick;

    public User(Socket s) throws IOException {

        this.socket = s;
        outServer = new DataOutputStream(socket.getOutputStream());
        inServer = new DataInputStream(socket.getInputStream());
        this.sc = new Scanner(System.in);

        System.out.println("Welcome!");
    }

    //Regista o cliente
    public void registar() throws IOException, NoSuchAlgorithmException {

        JsonObject json = new JsonObject();

        System.out.print("Nome: ");
        nick = sc.nextLine();

        //Colcoa nome no Json
        json.addProperty("command", "register");
        json.addProperty("src", nick);

        String jsonCommand = json.toString();

        //envia json para o server
        outServer.write(jsonCommand.getBytes());

        //Recebe a resposta
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String resposta = respostaServidor.readLine();
        JsonObject jsonResposta = new JsonParser().parse(resposta).getAsJsonObject();
        createKeys();

    }

    //lista os utilizadores
    public void listar() throws IOException {

        JsonObject json = new JsonObject();
        json.addProperty("command", "list");
        String jsonCommand = json.toString();

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        //Recebe a resposta
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String resposta = respostaServidor.readLine();
        JsonObject jsonResposta = new JsonParser().parse(resposta).getAsJsonObject();
        System.out.println(jsonResposta.get("result"));
    }

    public void diffieHellman() throws IOException, JSONException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException, InvalidParameterSpecException, SignatureException {

        System.out.println("Destinatario");
        String dst = sc.nextLine();

        JsonObject json = new JsonObject();
        json.addProperty("command", "send");
        json.addProperty("cifra", "DH");
        json.addProperty("src", nick);
        json.addProperty("dst", dst);
        String jsonCommand = json.toString();

        //Para mandar para o Servidor
        outServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        String resposta = respostaServidor.readLine();
        System.out.println(resposta);

        if (publickeys.containsKey(dst)) {
            otherPubicKey = publickeys.get(dst);
        }

        creatCommonKey(otherPubicKey);

        System.out.println("Mensagem: ");
        String msg = sc.nextLine();

        byte[] encMsg = encryptDHMensage(msg);

        //sendmessage
        sendDH(dst, encMsg);

    }

    public void sendPublicKey() throws IOException {

        JsonObject json = new JsonObject();
        json.addProperty("command", "send");
        json.addProperty("chavePublica", DatatypeConverter.printBase64Binary(publicKey.getEncoded()));
        json.addProperty("cifra", "publicKey");
        json.addProperty("src", nick);
        String jsonCommand = json.toString();

        //Para mandar para o Servidor
        outServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        String resposta = respostaServidor.readLine();
        System.out.println(resposta);
    }

    public void sendPublicKey(String dest) throws IOException {

        JsonObject json = new JsonObject();
        json.addProperty("command", "send");
        json.addProperty("chavePublica", DatatypeConverter.printBase64Binary(publicKey.getEncoded()));
        json.addProperty("cifra", "publicKeyDH");
        json.addProperty("src", nick);
        json.addProperty("dest", dest);
        String jsonCommand = json.toString();

        //Para mandar para o Servidor
        outServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        String resposta = respostaServidor.readLine();
        System.out.println(resposta);
    }

    public void sendMessage() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidParameterSpecException, JSONException, InvalidKeySpecException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException, SignatureException {

        System.out.print("Destinatario (enter to all): ");
        String destinatario = sc.nextLine();
        System.out.println();

        System.out.print("Mensagem: ");
        String msg = sc.nextLine();

        if (destinatario.equals("")) {
            sendAll(msg);
        } else {
            System.out.println("Tipo de cifra: "); //Pede ao utilizador o tipo de cifra a aplicar na mensagem
            System.out.println("    -> senha");
            System.out.println("    -> hibrida");
            System.out.println("    -> assinatura");

            String cipherType = sc.nextLine();

            switch (cipherType) {
                case "senha":
                    sendTo(destinatario, PassCipher(msg), cipherType);
                    break;
                case "hibrida":
                    String[] s = HybridCipher(msg, publickeys.get(destinatario));
                    sendToH(destinatario, s[1], s[0], cipherType);
                    break;

                case "assinatura":
                    sendSign(destinatario, msg);
                    break;
                default:
                    System.out.println("Opcao invalida");
                    break;
            }
        }
    }

    public void sendSign(String destinatario, String mensagem) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
        JsonObject json = new JsonObject();
        json.addProperty("command", "send");
        json.addProperty("dst", destinatario);

        PublicKey key = publickeys.get(destinatario);
        Cipher c = Cipher.getInstance("RSA");
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] cipherMsg = c.doFinal(mensagem.getBytes());

        json.addProperty("src", nick);
        json.addProperty("msg", DatatypeConverter.printBase64Binary(cipherMsg));
        json.addProperty("assinatura", assinar(mensagem));
        json.addProperty("cifra", "assinatura");
        String jsonCommand = json.toString();

        //Para mandar para o Servidor
        outServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        String resposta = respostaServidor.readLine();
        System.out.println(resposta);
    }

    //assinar uma mensagem
    private String assinar(String mensagem) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, NoSuchPaddingException {
        byte[] msg = mensagem.getBytes();

        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initSign(privateKey);
        signature.update(msg);
        byte[] sign = signature.sign();
        return DatatypeConverter.printBase64Binary(sign);
    }

    //verificar uma assinatura
    private boolean verificar(PublicKey publicKey, String msg, byte[] sign, String emissor) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        byte[] mensagem = DatatypeConverter.parseBase64Binary(msg);
        Cipher c = Cipher.getInstance("RSA");
        c.init(Cipher.DECRYPT_MODE, privateKey);
        mensagem = c.doFinal(mensagem);
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initVerify(publicKey);
        signature.update(mensagem);
        System.out.print(emissor + ":");
        System.out.println(new String(mensagem, "UTF-8"));
        return signature.verify(sign);
    }

    //funçao para iniciar uma sessao
    public void session() throws IOException, JSONException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException, InvalidParameterSpecException {

        System.out.print("1-Senha \n");
        System.out.print("2-Diffie-Hellman \n");
        System.out.print("3-Chave publica \n");
        String choice = sc.nextLine();
        System.out.print("Destinatario: ");
        String destinatario = sc.nextLine();

        if (choice.equals("1")) {

            System.out.print("Senha a usar: ");
            String senha = sc.nextLine();

            System.out.println("Sessao iniciada com " + destinatario);

            while (true) {

                String mensagem = sc.nextLine();

                if (mensagem.equals("terminar")) {
                    endSession(destinatario);
                    System.out.println("Sessao terminada");
                    break;
                }
                if (mensagem.equals("ler")) {

                    inServer = new DataInputStream(socket.getInputStream());

                    BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    byte[] read = new byte[1024];

                    inServer.read(read);

                    FileOutputStream fos = new FileOutputStream("Leitura");
                    String s = new String(read);

                    JSONObject object = new JSONObject(s);
                    String cipherType = object.getString("cifra");

                    if (cipherType.equals("sessionEnd")) {
                        System.out.println("Sessao terminada");
                        break;
                    }
                    String emissor = object.getString("src");
                    String iv = object.getString("iv");
                    mensagem = object.getString("msg");
                    System.out.println(emissor + ": " + PassDecryptsSession(mensagem, iv, senha));
                } else {
                    sendToSession(destinatario, PassCipherSession(mensagem, senha), "session");
                }
            }
        }

        if (choice.equals("3")) {

            KeyGenerator kg = KeyGenerator.getInstance("AES");
            kg.init(256);
            SecretKey secretK = kg.generateKey();
            byte[] keyBytes = secretK.getEncoded();

            Cipher c = Cipher.getInstance("RSA");
            c.init(Cipher.ENCRYPT_MODE, publickeys.get(destinatario));
            byte[] encryptedKey = c.doFinal(keyBytes);

            JsonObject json = new JsonObject();
            json.addProperty("command", "send");
            json.addProperty("src", nick);
            json.addProperty("dst", destinatario);
            json.addProperty("sharedKey", DatatypeConverter.printBase64Binary(encryptedKey));
            json.addProperty("cifra", "session3");
            String jsonCommand = json.toString();

            //Para mandar para o Servidor
            outServer = new DataOutputStream(socket.getOutputStream());
            BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            //Envia o JSon
            outServer.write(jsonCommand.getBytes());

            String resposta = respostaServidor.readLine();
            System.out.println("Sessao iniciada com " + destinatario);

            while (true) {
                String mensagem = sc.nextLine();
                if (mensagem.equals("terminar")) {
                    endSession(destinatario);
                    System.out.println("Sessao terminada");
                    break;
                }
                if (mensagem.equals("ler")) {
                    inServer = new DataInputStream(socket.getInputStream());
                    respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    byte[] read = new byte[1024];
                    inServer.read(read);
                    FileOutputStream fos = new FileOutputStream("Leitura");
                    String s = new String(read);
                    JSONObject object = new JSONObject(s);
                    String cipherType = object.getString("cifra");
                    if (cipherType.equals("sessionEnd")) {
                        System.out.println("Sessao terminada");
                        break;
                    }
                    String emissor = object.getString("src");
                    mensagem = object.getString("msg");
                    c = Cipher.getInstance("AES");
                    c.init(Cipher.DECRYPT_MODE, secretK);
                    byte[] f = c.doFinal(DatatypeConverter.parseBase64Binary(mensagem));
                    System.out.println(emissor + ": " + new String(f, "UTF-8"));
                } else {
                    session3(secretK, destinatario, mensagem);
                }
            }
        }
    }

    private void session3(SecretKey sk, String dest, String mensagem) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        byte[] byteMsg = DatatypeConverter.parseBase64Binary(mensagem);

        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.ENCRYPT_MODE, sk);
        byte[] encryptMsg = c.doFinal(mensagem.getBytes("UTF-8"));

        JsonObject json = new JsonObject();
        json.addProperty("command", "send");
        json.addProperty("src", nick);
        json.addProperty("dst", dest);
        json.addProperty("msg", DatatypeConverter.printBase64Binary(encryptMsg));
        json.addProperty("cifra", "session3");
        String jsonCommand = json.toString();

        //Para mandar para o Servidor
        outServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        String resposta = respostaServidor.readLine();

    }

    private void session3two(String destinatario, String sk) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, JSONException {
        Cipher c = Cipher.getInstance("RSA");
        c.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] skbytes = DatatypeConverter.parseBase64Binary(sk);
        byte[] key = c.doFinal(skbytes);

        SecretKey secretK = new SecretKeySpec(key, "AES");
        System.out.println("Sessao iniciada com " + destinatario);
        while (true) {
            String mensagem = sc.nextLine();
            if (mensagem.equals("terminar")) {
                endSession(destinatario);
                System.out.println("Sessao terminada");
                break;
            }
            if (mensagem.equals("ler")) {
                inServer = new DataInputStream(socket.getInputStream());
                BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                byte[] read = new byte[1024];
                inServer.read(read);
                FileOutputStream fos = new FileOutputStream("Leitura");
                String s = new String(read);
                JSONObject object = new JSONObject(s);
                String cipherType = object.getString("cifra");
                if (cipherType.equals("sessionEnd")) {
                    System.out.println("Sessao terminada");
                    break;
                }
                String emissor = object.getString("src");
                mensagem = object.getString("msg");
                c = Cipher.getInstance("AES");
                c.init(Cipher.DECRYPT_MODE, secretK);
                byte[] f = c.doFinal(DatatypeConverter.parseBase64Binary(mensagem));
                System.out.println(emissor + ": " + new String(f, "UTF-8"));

            } else {
                session3(secretK, destinatario, mensagem);
            }
        }

    }

    //metedo para iniciar uma sessao no lado recetor
    private void sessionTwo(String destinatario, String senha) throws IOException, JSONException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException, InvalidParameterSpecException {
        System.out.println("Sessao iniciada com " + destinatario);
        while (true) {
            String mensagem = sc.nextLine();
            if (mensagem.equals("terminar")) {
                endSession(destinatario);
                System.out.println("Sessao terminada");
                break;
            }
            if (mensagem.equals("ler")) {
                inServer = new DataInputStream(socket.getInputStream());
                BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                byte[] read = new byte[1024];
                inServer.read(read);
                FileOutputStream fos = new FileOutputStream("Leitura");
                String s = new String(read);
                JSONObject object = new JSONObject(s);
                String cipherType = object.getString("cifra");
                if (cipherType.equals("sessionEnd")) {
                    System.out.println("Sessao terminada");
                    break;
                }
                String emissor = object.getString("src");
                String iv = object.getString("iv");
                mensagem = object.getString("msg");
                System.out.println(emissor + ": " + PassDecryptsSession(mensagem, iv, senha));
            } else {
                sendToSession(destinatario, PassCipherSession(mensagem, senha), "session");
            }
        }
    }

    private void sendAll(String msg) throws IOException {

        JsonObject json = new JsonObject();
        json.addProperty("command", "send");
        json.addProperty("msg", msg);
        String jsonCommand = json.toString();

        //Para mandar para o Servidor
        outServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        String resposta = respostaServidor.readLine();
    }

    //envio de mensagem para terminar sessao
    private void endSession(String dest) throws IOException {
        JsonObject json = new JsonObject();
        json.addProperty("command", "send");
        json.addProperty("dst", dest);
        json.addProperty("cifra", "sessionEnd");
        String jsonCommand = json.toString();

        //Para mandar para o Servidor
        outServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        String resposta = respostaServidor.readLine();
    }

    //send com metedo senha
    private void sendTo(String dest, byte[] msg, String cifra) throws IOException {
        JsonObject json = new JsonObject();
        json.addProperty("command", "send");
        json.addProperty("src", nick);
        json.addProperty("dst", dest);
        json.addProperty("msg", DatatypeConverter.printBase64Binary(msg));
        json.addProperty("iv", DatatypeConverter.printBase64Binary(iv));
        json.addProperty("cifra", cifra);
        String jsonCommand = json.toString();

        //Para mandar para o Servidor
        outServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        String resposta = respostaServidor.readLine();
    }

    //Send cifra com passe em sessao
    private void sendToSession(String dest, byte[] msg, String cifra) throws IOException {
        JsonObject json = new JsonObject();
        json.addProperty("command", "send");
        json.addProperty("src", nick);
        json.addProperty("dst", dest);
        json.addProperty("msg", DatatypeConverter.printBase64Binary(msg));
        json.addProperty("iv", DatatypeConverter.printBase64Binary(iv));
        json.addProperty("cifra", cifra);
        String jsonCommand = json.toString();

        //Para mandar para o Servidor
        outServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        String resposta = respostaServidor.readLine();
    }

    //send com hibrida
    private void sendToH(String dest, String chave, String msg, String cifra) throws IOException {
        JsonObject json = new JsonObject();
        json.addProperty("command", "send");
        json.addProperty("src", nick);
        json.addProperty("dst", dest);
        json.addProperty("msg", msg);
        json.addProperty("chave simetrica", chave);
        json.addProperty("cifra", cifra);
        String jsonCommand = json.toString();

        outServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        String resposta = respostaServidor.readLine();
        System.out.println(resposta);
    }

    public void receive() throws IOException, JSONException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException, InvalidParameterSpecException, SignatureException {
        inServer = new DataInputStream(socket.getInputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        byte[] read = new byte[1024];
        inServer.read(read);

        FileOutputStream fos = new FileOutputStream("Leitura");
        String s = new String(read);
        JSONObject object = new JSONObject(s);
        String cipherType = object.getString("cifra");

        switch (cipherType) {
            case "senha":
                String emi = object.getString("src");
                String iv = object.getString("iv");
                String mensagem = object.getString("msg");
                System.out.println(emi + ":" + PassDecrypts(mensagem, iv));
                break;

            case "hibrida":
                String em = object.getString("src");
                String msg = object.getString("msg");
                String chaveSimetrica = object.getString("chave simetrica");
                System.out.println(em + ":" + HybridDecrypt(DatatypeConverter.parseBase64Binary(chaveSimetrica), DatatypeConverter.parseBase64Binary(msg)));
                break;

            case "publicKey":
                String pk = object.getString("chavePublica");
                String source = object.getString("src");
                X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(DatatypeConverter.parseBase64Binary(pk));
                KeyFactory keyFact = KeyFactory.getInstance("RSA");
                PublicKey p = keyFact.generatePublic(x509KeySpec);
                publickeys.put(source, p);
                break;

            case "session":
                String emissor = object.getString("src");
                iv = object.getString("iv");
                mensagem = object.getString("msg");
                System.out.print("Insira a senha para entrar em sessao ");
                String senha = sc.nextLine();
                System.out.println(emissor + ": " + PassDecryptsSession(mensagem, iv, senha));
                sessionTwo(emissor, senha);
                break;

            case "session3":
                String destinatario = object.getString("src");
                String chave = object.getString("sharedKey");
                session3two(destinatario, chave);
                break;

            case "assinatura":
                String remetente = object.getString("src");
                String data = object.getString("msg");
                String assinatura = object.getString("assinatura");
                byte[] sign = DatatypeConverter.parseBase64Binary(assinatura);
                PublicKey pKey = publickeys.get(remetente);

                if (verificar(pKey, data, sign, remetente)) {
                    System.out.println("Documento verificado com sucesso");
                } else {
                    System.err.println("Documento FALSIFICADO");
                }
                break;

            default:
                System.out.print("ERROR");
                break;
        }
    }

    private void createKeys() throws NoSuchAlgorithmException {

        KeyPairGenerator kpg = KeyPairGenerator.getInstance(ALGORITIMO);
        kpg.initialize(ALGORITIMO_LENGTH);
        KeyPair kp = kpg.generateKeyPair();
        publicKey = kp.getPublic();
        privateKey = kp.getPrivate();
    }

    public void respostaServer(String resposta) {

        JsonObject jsonResposta = new JsonParser().parse(resposta).getAsJsonObject();
        String finalResposta = jsonResposta.get("msg").toString().substring(0, jsonResposta.toString().length() - 1);
    }

    //Funcao para encriptar no modo senha
    private byte[] PassCipher(String mensagem) throws NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchPaddingException, InvalidParameterSpecException, UnsupportedEncodingException {

        boolean label;
        String senha;

        System.out.print("Senha a usar: ");
        do {
            senha = sc.nextLine();
            label = isValidPassword(senha);
        } while (label);

        byte[] passByte = senha.getBytes("UTF-8");
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        byte[] passDigest = messageDigest.digest(passByte);
        passDigest = Arrays.copyOf(passDigest, 8);
        SecretKeySpec sk = new SecretKeySpec(passDigest, "DES");

        Cipher ci = Cipher.getInstance("DES/CBC/PKCS5Padding");
        ci.init(Cipher.ENCRYPT_MODE, sk);
        AlgorithmParameters params = ci.getParameters();
        iv = params.getParameterSpec(IvParameterSpec.class).getIV();

        byte[] cipheredMess = ci.doFinal(mensagem.getBytes("UTF-8"));
        return cipheredMess;
    }

    private byte[] PassCipherSession(String mensagem, String senha) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException {

        byte[] passByte = senha.getBytes("UTF-8");
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        byte[] passDigest = messageDigest.digest(passByte);
        passDigest = Arrays.copyOf(passDigest, 8);
        SecretKeySpec sk = new SecretKeySpec(passDigest, "DES");

        Cipher ci = Cipher.getInstance("DES/CBC/PKCS5Padding");
        ci.init(Cipher.ENCRYPT_MODE, sk);
        AlgorithmParameters params = ci.getParameters();
        iv = params.getParameterSpec(IvParameterSpec.class).getIV();

        byte[] cipheredMess = ci.doFinal(mensagem.getBytes("UTF-8"));
        return cipheredMess;
    }

    //Funcao para desencriptar no modo senha
    private String PassDecrypts(String m, String i) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {

        System.out.print("Senha a usar: ");
        String senha = sc.nextLine();
        byte[] mensagem = DatatypeConverter.parseBase64Binary(m);
        byte[] iv = DatatypeConverter.parseBase64Binary(i);

        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        byte[] passByte = senha.getBytes("UTF-8");
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        byte[] passDigest = messageDigest.digest(passByte);
        passDigest = Arrays.copyOf(passDigest, 8);
        SecretKeySpec sec = new SecretKeySpec(passDigest, "DES");

        cipher.init(Cipher.DECRYPT_MODE, sec, new IvParameterSpec(iv));
        String plaintext = new String(cipher.doFinal(mensagem), "UTF-8");
        return plaintext;
    }

    private String PassDecryptsSession(String m, String i, String senha) throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        byte[] mensagem = DatatypeConverter.parseBase64Binary(m);
        byte[] iv = DatatypeConverter.parseBase64Binary(i);

        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        byte[] passByte = senha.getBytes("UTF-8");
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        byte[] passDigest = messageDigest.digest(passByte);
        passDigest = Arrays.copyOf(passDigest, 8);
        SecretKeySpec sec = new SecretKeySpec(passDigest, "DES");

        cipher.init(Cipher.DECRYPT_MODE, sec, new IvParameterSpec(iv));
        String plaintext = new String(cipher.doFinal(mensagem), "UTF-8");
        return plaintext;
    }

    //Funcao para encriptar no modo hibrido
    private String[] HybridCipher(String msg, PublicKey pubKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException { //deve retornar duas encriptaçoes
        //Geraçao de uma chave simetrica de tamanho 256 usando o algoritmo de AES
        KeyGenerator kg = KeyGenerator.getInstance("AES");
        kg.init(256);
        secretKey = kg.generateKey();

        byte[] message = msg.getBytes();

        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptMsg = c.doFinal(message); //Mensagem encriptada com a chave simetrica

        c = Cipher.getInstance("RSA");
        c.init(Cipher.ENCRYPT_MODE, pubKey);
        byte[] keyBytes = secretKey.getEncoded();
        byte[] encryptKey = c.doFinal(keyBytes); //chave simetrica encriptada com a chave publica

        String[] s = new String[2];
        s[0] = DatatypeConverter.printBase64Binary(encryptMsg);
        s[1] = DatatypeConverter.printBase64Binary(encryptKey);

        return s;
    }

    //Funçao para desencriptar no modo hibrido ->
    private String HybridDecrypt(byte[] encryptKey, byte[] encryptMsg) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {

        Cipher c = Cipher.getInstance("RSA");
        c.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] k = c.doFinal(encryptKey);

        SecretKey key = new SecretKeySpec(k, "AES"); //Chave simetrica recebida com a mensagem

        c = Cipher.getInstance("AES");
        c.init(Cipher.DECRYPT_MODE, key);
        //byte[] decryptedMsg = c.doFinal(encryptMsg); //Mensagem desencriptada
        return new String(c.doFinal(encryptMsg), "UTF-8");

    }

    private boolean isValidPassword(String senha) {

        if (usedPassword.contains(senha.hashCode())) {
            System.err.println("Password já foi utilizada!");
            System.err.print("Introduza uma nova senha: ");
            return true;
        }

        usedPassword.add(senha.hashCode());
        System.out.println("Mensagem enviada");
        return false;
    }

    private void creatCommonKey(PublicKey otherPubicKey) {
        try {
            KeyAgreement keyAgreement = KeyAgreement.getInstance("DH");
            keyAgreement.init(privateKey);
            keyAgreement.doPhase(otherPubicKey, true);

            dhSecretKey = shortenSecretKey(keyAgreement.generateSecret());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private byte[] shortenSecretKey(byte[] key) {
        try {

            // 8 bytes(64 bits) for DES
            // 6 bytes(48 bits) for Blowfish
            /*final byte[] shortKey = new byte[8];

            System.arraycopy(secretKey, 0, shortKey, 0, shortKey.length);

            return shortKey;*/
            // Below lines can be more secure
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            DESKeySpec desSpec = new DESKeySpec(key);

            return keyFactory.generateSecret(desSpec).getEncoded();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private byte[] encryptDHMensage(String msg) {

        try {

            final SecretKeySpec keySpec = new SecretKeySpec(dhSecretKey, "DES");
            final Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");

            cipher.init(Cipher.ENCRYPT_MODE, keySpec);

            byte[] encryptedMessage = cipher.doFinal(msg.getBytes());

            return encryptedMessage;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void sendDH(String dest, byte[] msg) throws IOException {
        JsonObject json = new JsonObject();
        json.addProperty("command", "send");
        json.addProperty("dst", dest);
        json.addProperty("msg", DatatypeConverter.printBase64Binary(msg));
        json.addProperty("cifra", "DH");
        String jsonCommand = json.toString();

        //Para mandar para o Servidor
        outServer = new DataOutputStream(socket.getOutputStream());
        BufferedReader respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        //Envia o JSon
        outServer.write(jsonCommand.getBytes());

        String resposta = respostaServidor.readLine();
        System.out.println(resposta);
    }

}
