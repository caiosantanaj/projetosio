package ServerPackage;

import java.io.IOException;
import java.net.Socket;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.json.JSONException;

class Client {
    
    public static Scanner sc = new Scanner(System.in);
    private static User user;
    
    public static void main(String[] args) {
        Socket s;
        OutputStream out;
        InputStream inputStream;
        byte[] buffer = new byte[1024];
        int porta = 8080;
        
        try {
            s = new Socket("localhost", porta);
            user = new User(s);
            out = s.getOutputStream();
            inputStream = s.getInputStream();
            
            while (true) {
                
                menu();
                
                int l;
                if (System.in.available() != 0) {
                    l = System.in.read(buffer);
                    
                    if (l == -1) {
                        break;
                    }
                    out.write(buffer, 0, l);
                }
                if (inputStream.available() != 0) {
                    l = inputStream.read(buffer, 0, buffer.length);
                    System.out.write(buffer, 0, l);
                    //user.respostaServer(buffer.toString());
                    System.out.print("\n");
                }
                
                Thread.currentThread().sleep(200); // 100 milis
            }
        } catch (Exception e) {
            System.err.println("Exception: " + e);
        }
    }
    
    private static void menu() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, JSONException, InvalidKeySpecException, InvalidParameterSpecException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException, SignatureException {
        
        System.out.println("Opcoes:");
        System.out.println("    1-registar");
        System.out.println("    2-enviar chave publica");
        System.out.println("    3-listar");
        System.out.println("    4-mensagem");
        System.out.println("    5-session");
        System.out.println("    6-ler");
        System.out.println("    exit");
        
        String op = sc.nextLine();
        
        switch (op) {
            case "registar":
                registar();
                break;
                
            case "1":
                registar();
                break;
            
            case "2":
                sendPublicKey();
                break;
            case "enviar chave publica":
                break;
                
            case "3":
                listar();
                break;
                
            case "listar":
                listar();
                break;
                
            case "mensagem":
                sendMessage();
                break;
                
            case "4":
                sendMessage();
                break;
                
            case "session":
                session();
                break;
                
            case "5":
                session();
                break;
                
            case "ler":
                receive();
                break;
                
            case "6":
                receive();
                break;
                
            case "exit":
                System.exit(0);
                break;
                
            default:
                System.out.println("invalida");
                break;
                
        }
    }
    
    //Receber(tmp)--------------------------------------------------------------
    private static void receive() throws IOException, JSONException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException, InvalidParameterSpecException, SignatureException {
        user.receive();
    }
    
    private static void registar() throws IOException, NoSuchAlgorithmException {
        user.registar();
    }
    
    private static void listar() throws IOException {
        user.listar();
    }
    
    private static void sendMessage() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidParameterSpecException, JSONException, InvalidKeySpecException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException, SignatureException {
        user.sendMessage();
    }
    
    private static void session() throws IOException, JSONException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchProviderException, InvalidParameterSpecException {
        user.session();
    }
    
    private static void sendPublicKey() throws IOException{
        user.sendPublicKey();
    }
}