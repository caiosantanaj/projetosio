/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package ServerPackage;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.System.in;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class myClient extends Thread {
    
    private final Socket socket;
    private JsonReader inServer;
    BufferedReader respostaServidor;
    
    public myClient(Socket socket) throws IOException {
        this.socket = socket;
    }
    
    @Override
    public void run() {
        
        while (true) {
            try {
                
                respostaServidor = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String resposta = respostaServidor.readLine();
                JsonObject jsonResposta = new JsonParser().parse(resposta).getAsJsonObject();
                
            } catch (IOException ex) {
                Logger.getLogger(myClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
