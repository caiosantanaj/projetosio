package ServerPackage;

import java.security.AlgorithmParameterGenerator;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidParameterSpecException;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;

public class AcordoDiffieHellman {
    
    private DHParameterSpec dhParamSpec;
    private PrivateKey privateKey;
    private PublicKey publicKey;
    private PublicKey publicKeyB;
    private PublicKey otherPubKey;
    
    KeyAgreement userKeyAgree;
    
    public AcordoDiffieHellman() throws NoSuchAlgorithmException, InvalidParameterSpecException, InvalidAlgorithmParameterException, InvalidKeyException {
        dhParamSpec = null;
        criarParametros();
        criarDHKey();
    }
    
    public AcordoDiffieHellman(PublicKey otherPublicKey) {
        this.otherPubKey = otherPublicKey;
        dhParamSpec = ((DHPublicKey)otherPublicKey).getParams();
    }
    
    
    private void criarParametros() throws NoSuchAlgorithmException, InvalidParameterSpecException {

        AlgorithmParameterGenerator paramGen
                = AlgorithmParameterGenerator.getInstance("DH");
        paramGen.init(512);
        AlgorithmParameters params = paramGen.generateParameters();
        dhParamSpec = (DHParameterSpec)params.getParameterSpec(DHParameterSpec.class);
    }
    
    private void criarDHKey() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("DH");
        keyGenerator.initialize(dhParamSpec);
        KeyPair userKeyPar = keyGenerator.generateKeyPair();
        
        userKeyAgree = KeyAgreement.getInstance("DH");
        privateKey = userKeyPar.getPrivate();
        publicKey = userKeyPar.getPublic();
        userKeyAgree.init(privateKey);
    }
    
    private void criarDHKey(DHParameterSpec dhParamRec) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        
        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("DH");
        keyGenerator.initialize(dhParamRec);
        KeyPair userKeyPar = keyGenerator.generateKeyPair();
        
        userKeyAgree = KeyAgreement.getInstance("DH");
        privateKey = userKeyPar.getPrivate();
        publicKey = userKeyPar.getPublic();
        userKeyAgree.init(privateKey);
    }
    
    public byte[] getPubKeyEnc() {
        return publicKey.getEncoded();
    }
    
    public void startAccord(PublicKey publicKeyB) throws InvalidKeyException {
        
        this.publicKeyB = publicKeyB;
        userKeyAgree.doPhase(publicKeyB, true);
        generateSecret();
    }

    public int generateSecret() {
        byte[] userSecretKey = userKeyAgree.generateSecret();
        return userSecretKey.length;
    }
    
}
